let add_button = document.querySelector('add-button');
let like = document.querySelector('like');
let container = document.querySelector('container');

add_button.onclick = function () {
    add_button.classList.toggle('add-button');
    add_button.classList.toggle('add-button-on-click');
};

like.onclick = function () {
    like.classList.toggle('like');
    like.classList.toggle('like-on-click');
};

container.onmouseover = function () {
    container.classList.add('move');
    container.classList.remove('remove');
};

container.onmouseout = function () {
    container.classList.remove('move');
    container.classList.add('remove');
};